use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use actix_web::dev::Server;
use std::process;

async fn health_check() -> HttpResponse {
    println!("panic");
    HttpResponse::Ok().finish()
}

async fn death() -> impl Responder {
    let exit_code = 42069;
    println!("I AM COMMIT DEATH!");
    process::exit(exit_code);
    HttpResponse::Ok()
}

#[tokio::main]
pub async fn run() -> Result<Server, std::io::Error> {
    let server = HttpServer::new(|| {
        App::new()
            .route("/health_check", web::get().to(health_check))
            .route("/death", web::get().to(death))
    })
    .bind("0.0.0.0:8000")?
    .run();
    Ok(server)
}
